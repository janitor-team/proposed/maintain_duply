Package:     duply
Subject:     reduce usage text in script
Author:      Joachim Wiedorn <joodebian at joonet.de>
Forwarded:   no
Last-Update: 2018-11-17

All infos are inside the executable script. I have copied the
manpage text from the bash script into an extra manpage file
for better usability. I have leaved only some pieces of the
original text for quick overview of usage.
---

diff -urN s04/duply s05/duply
--- s04/duply	2019-09-23 20:18:07.624190286 +0200
+++ s05/duply	2019-09-23 20:26:06.113638258 +0200
@@ -242,26 +242,12 @@
   
 DESCRIPTION: 
   Duply deals as a wrapper for the mighty duplicity magic.
-  It simplifies running duplicity with cron or on command line by:
+  It simplifies running duplicity with cron or on command line.
 
-    - keeping recurring settings in profiles per backup job
-    - enabling batch operations eg. backup_verify+purge
-    - executing pre/post scripts (different actions possible 
-      depending on previous or next command or it's exit status)
-    - precondition checking for flawless duplicity operation
-
-  For each backup job one configuration profile must be created.
-  The profile folder will be stored under '~/.${ME_NAME}/<profile>'
-  (where ~ is the current users home directory).
-  Hint:  
-   If the folder '/etc/${ME_NAME}' exists, the profiles for the super
-   user root will be searched & created there.
-
-USAGE:
-  first time usage (profile creation):  
+  First time usage (profile creation):
     $ME <profile> create
 
-  general usage in single or batch mode (see EXAMPLES):  
+  General usage in single or batch mode (see EXAMPLES):
     $ME <profile> <command>[[_|+|-]<command>[_|+|-]...] [<options> ...]
 
   For batches the conditional separators can also be written as pseudo commands
@@ -270,26 +256,6 @@
   Non $ME options are passed on to duplicity (see OPTIONS).
   All conf parameters can also be defined in the environment instead.
 
-PROFILE:
-  Indicated by a path or a profile name (<profile>), which is resolved 
-  to '~/.${ME_NAME}/<profile>' (~ expands to environment variable \$HOME).
-
-  Superuser root can place profiles under '/etc/${ME_NAME}'. Simply create
-  the folder manually before running $ME_NAME as superuser.
-  Note:  
-    Already existing profiles in root's home folder will cease to work
-    unless they are moved to the new location manually.
-
-  example 1:   $ME humbug backup
-
-  Alternatively a _path_ might be used e.g. useful for quick testing, 
-  restoring or exotic locations. Shell expansion should work as usual.
-  Hint:  
-    The path must contain at least one path separator '/', 
-    e.g. './test' instead of only 'test'.
-
-  example 2:   $ME ~/.${ME_NAME}/humbug backup
-
 SEPARATORS:
   _ (underscore)  
              neutral separator
@@ -350,9 +316,6 @@
               [use --force to actually delete these files]
 
   changelog  print changelog / todo list
-  txt2man    feature for package maintainers - create a manpage based on the 
-             usage output. download txt2man from http://mvertes.free.fr/, put 
-             it in the PATH and run '$ME txt2man' to create a man page.
   version    show version information of $ME_NAME and needed programs
 
 OPTIONS:
@@ -370,52 +333,10 @@
     12D (interval, 12 days ago)
     1h78m (interval, 1 hour 78 minutes ago)
 
-PRE/POST SCRIPTS:
-  Some useful internal duply variables are exported to the scripts.
-
-    PROFILE, CONFDIR, SOURCE, TARGET_URL_<PROT|HOSTPATH|USER|PASS>, 
-    GPG_<KEYS_ENC|KEY_SIGN|PW>, CMD_ERR, RUN_START,
-    CMD_<PREV|NEXT> (previous/next command), 
-    CND_<PREV|NEXT> (condition before/after)
-
-  The CMD_* variables were introduced to allow different actions according to 
-  the command the scripts were attached to e.g. 'pre_bkp_post_pre_verify_post' 
-  will call the pre script two times, with CMD_NEXT variable set to 'bkp' 
-  on the first and to 'verify' on the second run.
-  CMD_ERR holds the exit code of the CMD_PREV .
-
-EXAMPLES:
-  create profile 'humbug':  
-    $ME humbug create (don't forget to edit this new conf file)
-  backup 'humbug' now:  
-    $ME humbug backup
-  list available backup sets of profile 'humbug':  
-    $ME humbug status
-  list and delete outdated backups of 'humbug':  
-    $ME humbug purge --force
-  restore latest backup of 'humbug' to /mnt/restore:  
-    $ME humbug restore /mnt/restore
-  restore /etc/passwd of 'humbug' from 4 days ago to /root/pw:  
-    $ME humbug fetch etc/passwd /root/pw 4D
-    (see "duplicity manpage", section TIME FORMATS)
-  a one line batch job on 'humbug' for cron execution:  
-    $ME humbug backup_verify_purge --force
-  batch job to run a full backup with pre/post scripts:  
-    $ME humbug pre_full_post
-
-FILES:
-  in profile folder '~/.${ME_NAME}/<profile>' or '/etc/${ME_NAME}'
-  conf             profile configuration file
-  pre,post         pre/post scripts (see above for details)
-  gpgkey.*.asc     exported GPG key files
-  exclude          a globbing list of included or excluded files/folders
-                   (see "duplicity manpage", section FILE SELECTION)
-
 $(hint_profile)
 
 SEE ALSO:
-  duplicity man page:
-    duplicity(1) or http://duplicity.nongnu.org/duplicity.1.html
+    man pages: duply(1), duplicity(1)
 USAGE_EOF
 }
 
